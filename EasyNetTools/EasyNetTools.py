import re
import sys
import time
import ftplib
import smtplib
import paramiko
import telnetlib
import subprocess
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.message import MIMEMessage
from email.mime.multipart import MIMEMultipart
from multiprocessing import Value
from datetime import date, timedelta
paramiko.util.log_to_file('/dev/null')

"""
This libray was made to simplfy normal tasks of a network admin.
This will take care of ssh/telnet, SFTP, E-mail, and some other common
functions while keeping connections to routers interactive on the script
that is calling it.

Contact: patrick.lawless@charter.com
"""

class EasyNetTools:
        def __init__(self, **kwargs):
            self.dontprint = (['show', 'enable', 'Password:', 'terminal', 'screen-length',
                 'Screen length', 'environment no more', '{master', 'Building config',
                 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'terminal len 0'])
            self.Host = kwargs.get('Host', None)
            self.Username = kwargs.get('Username', None)
            self.Password = kwargs.get('Password', None)
            self.Sec_Username = kwargs.get('Sec_Username', None)
            self.Sec_Password = kwargs.get('Sec_Password', None)
            self.Recipient = kwargs.get('Recipient', None)
            self.MailSender = kwargs.get('MailSender', 'test@nettools.com')
            self.MailSubject = kwargs.get('MailSubject', 'Nettools E-mail')
            self.MailBody = kwargs.get('MailBody', None)
            self.MailFiles = kwargs.get('MailFiles', None)
            self.MailFormat = kwargs.get('MailFormat', 'Plain')
            self.WorkingDir = kwargs.get('WorkingDir', '')
            self.CommandDelay = kwargs.get('CommandDelay', 5)
            self.alucommands= kwargs.get('alucommands', None)
            self.xrcommands= kwargs.get('xrcommands', None)
            self.asrcommands= kwargs.get('asrcommands', None)
            self.ioscommands= kwargs.get('ioscommands', None)
            self.junoscommands= kwargs.get('junoscommands', None)
            self.asacommands= kwargs.get('asacommands', None)
            self.linuxcommands= kwargs.get('linuxcommands', None)
            self.sshcounter = 0
            self.telnetcounter = 0
            self.sshcounter = Value('i', 0)
            self.telnetcounter = Value('i', 0)
            return


        def CommandFile(self):
            commandfiles = 0
            scripttime=[]
            try:
                self.alucommands = list(open(self.WorkingDir + 'ALU', 'r'))
                self.alucommands.insert(0, 'environment no more')
                self.scripttime.append(sum(1 for line in self.alucommands))
            except: commandfiles + 1
            try:
                self.xrcommands = list(open(self.WorkingDir + 'XR', 'r'))
                self.xrcommands.insert(0, 'terminal len 0')
                scripttime.append(sum(1 for line in self.xrcommands))
            except: commandfiles + 1
            try:
                self.asrcommands = list(open(self.WorkingDir + 'ASR', 'r'))
                self.asrcommands.insert(0, 'terminal len 0')
                scripttime.append(sum(1 for line in self.asrcommands))
            except: commandfiles + 1
            try:
                self.ioscommands = list(open(self.WorkingDir + 'IOS', 'r'))
                self.ioscommands.insert(0, 'terminal len 0')
                scripttime.append(sum(1 for line in self.ioscommands))
            except: commandfiles + 1
            try:
                self.junoscommands = list(open(self.WorkingDir + 'JUNOS', 'r'))
                self.junoscommands.insert(0, 'set cli screen-length 0')
                scripttime.append(sum(1 for line in self.junoscommands))
            except: commandfiles + 1
            try:
                self.asacommands = list(open(self.WorkingDir + 'ASA', 'r'))
                self.asacommands.insert(0, 'enable')
                self.asacommands.insert(1, self.Password)
                self.asacommands.insert(2, 'terminal pager 0')
                scripttime.append(sum(1 for line in self.asacommands))
            except: commandfiles + 1
            try:
                self.linuxcommands = list(open(self.WorkingDir + 'LINUX', 'r'))
                scripttime.append(sum(1 for line in self.linuxcommands))
            except: commandfiles + 1
            if commandfiles == 6:
                print("Couldn't find command files\r")
                sys.exit()
            try: 
                scripttime = max(scripttime)*self.CommandDelay + 10
            except:
                scripttime = 30
            CommandFile = ({'alucommands':self.alucommands,'xrcommands':self.xrcommands,
                'asrcommands':self.asrcommands,'ioscommands':self.ioscommands,
                'junoscommands':self.junoscommands,'asacommands':self.asacommands,
                'linuxcommands':self.linuxcommands,'scripttime':scripttime})
            return CommandFile


        def Login(self,Host):
            Host = Host.strip('\n')
            attempts = 0
            Attempt = 0
            while attempts < 2:  #Helps fix random connection failures
                try:
                    sshrun = self.sshwork(Host,Attempt)
                    return sshrun
                except:
                    time.sleep(8)
                    attempts += 1
            if attempts == 2:
                telnetrun = self.telnetwork(Host,Attempt)
                if telnetrun['commands'] != '1':
                    self.telnetcounter.value += 1
                    return telnetrun
                elif telnetrun['commands'] == '1':
                    if self.Sec_Username == None:
                        print("Could not log into - " + Host)
                        pass
                    else:
                        Attempt = 1
                        try:
                            #2nd Login
                            sshrun = self.sshwork(Host,Attempt)
                            return sshrun
                        except:
                            telnetrun = self.telnetwork(Host,Attempt)
                            self.telnetcounter.value += 1
                            return telnetrun


        def sshwork(self,Host,Attempt):   #SSH specific login and processing
            Host_Split = Host.split('.')[0]
            self.dontprint.append(Host)
            self.dontprint.append(Host.upper())
            self.dontprint.append(Host_Split)
            self.dontprint.append(Host_Split.upper())  
            sshsetup = paramiko.SSHClient() # Create instance of SSHClient object
            sshsetup.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            if Attempt == 0:
                sshsetup.connect(Host, username=self.Username, password=self.Password, look_for_keys=False, allow_agent=False, timeout=30)
            elif Attempt == 1:
                sshsetup.connect(Host, username=self.Sec_Username, password=self.Sec_Password, look_for_keys=False, allow_agent=False, timeout=30)
            session = sshsetup.invoke_shell(width=800)
            time.sleep(4)  #Allow time to log in and strip MOTD
            output = session.recv(10000).decode(errors="ignore")
            host_prompt = output.split('\n')[-1]
            expect_vty = self.expect_prompt(host_prompt)  # Builds list of expect options
            if re.search('[A-B]:.*#', output):software = 0 #ALU
            elif re.search(self.Username + '@.*\$', output):software = 5  #Linux
            elif re.search('CPU.*#', output):software = 1  #XR
            elif re.search('.*#', output):software = 2     #IOS
            elif re.search(self.Username + '@.*>', output):software = 3  #JUNOS
            elif re.search('.*>', output):software = 4  #ASA
            sshtransport = session.get_transport() #Debug SSH channel
            send = session.send
            commands = self.commandsdef(software, Host)
            session.keep_this = sshsetup
            data =({'send':send,'read':1,'session':session,'dontprint':self.dontprint,
                'commands':commands['commands'],'software':commands['software'],'expect':expect_vty})
            self.sshcounter.value += 1
            return data


        def telnetwork(self,Host,Attempt):   #Telnet specific login and processing
#            try:
                print("Trying Telnet on - "+ Host)
                session = telnetlib.Telnet(Host,23,20)
                session.expect([b'sername:',b'ogin'],5)
                if Attempt == 0:
                    session.write(self.Username.encode() + b'\r')
                    session.read_until(b'password:',6)
                    session.write(self.Password.encode() + b'\r')
                    software, match, previous_text = session.expect([b'[A-B]:.*#' ,b'CPU.*#' , b'.*#' , self.Username.encode() + b'@.*>'], 7)
                elif Attempt == 1:
                    session.write(self.Sec_Username.encode() + b'\r')
                    session.read_until(b'password:',6)
                    session.write(self.Sec_Password.encode() + b'\r')
                    software, match, previous_text = session.expect([b'[A-B]:.*#' ,b'CPU.*#' , b'.*#' , self.Sec_Username.encode() + b'@.*>'], 7)
                self.dontprint.append(previous_text.decode().split('\n')[-1].strip())
                send = session.write
                commands = self.commandsdef(software, Host)
                pt =previous_text.decode(errors="ignore")
                host_prompt = pt.split('\n')[-1]
                expect_vty = self.expect_prompt(host_prompt) # Builds list of expect options
                data =({'send':send,'read':2,'session':session,'dontprint':self.dontprint,
                        'commands':commands['commands'],'software':commands['software'],'expect':expect_vty})
                return data
 #           except:
 #               if Attempt == 0:
 #                   print("Could not log into - " + Host)
 #               if Attempt == 1:  
 #                   print("2nd Login Faild - " + Host)
 #                   pass


        def vty_output(self,session):
            # Expect like function to return 
            output = []
            out_page = []
            flag = 0
            data = None
            temp_data =''
            # SSH read
            if session['read'] == 1:
                data = session['session'].recv(500000).decode(errors="ignore").split('\n') 
                out_page.append(data)
                # If expect is not compleated will keep polling
#                while out_page[-1][-1] != session['expect']:
                while not out_page[-1][-1] in session['expect']:
                    flag = 1
                    time.sleep(1)
                    data = session['session'].recv(500000).decode(errors="ignore").split('\n')
                    out_page.append(data)
            # Telnet read
            else:
                data = session['session'].read_very_eager().decode(errors="ignore").split('\n')
                out_page.append(data)
                # If expect is not compleated will keep polling
#                while out_page[-1][-1] != session['expect']:
                while not out_page[-1][-1] in session['expect']:
                    flag = 1
                    time.sleep(1)
                    data = session['session'].read_very_eager().decode(errors="ignore").split('\n')
                    out_page.append(data)
            if flag == 0:
                output = out_page[-1]
            if flag == 1:
                for l in out_page:
                    for x in l:
                        temp_data+=x
            temp_data = temp_data.split('\r')
            # REMOVE '' FROM RETURN DATA
            if temp_data == ['']:pass
            elif temp_data[-1] =='':          
                del temp_data[-1]
                for l in temp_data:
                    output.append(l)  
            else:
                for l in temp_data:
                    output.append(l)
            return(output)


        def expect_prompt(self,host_prompt):
            expect_vty = [host_prompt]
            expect_vty.append(host_prompt.replace('#','(config)#'))  #Cisco Config
            expect_vty.append(host_prompt.replace('>','#'))          #Junos Config
            expect_vty.append(host_prompt.replace('#','>config#'))   #Nokia Config
            return(expect_vty)


        def sftp(self,Host,remotepath,localpath,option):
            port = 22
            transport = paramiko.Transport((Host, port))
            transport.connect(username = self.Username, password = self.Password)
            sftp = paramiko.SFTPClient.from_transport(transport)
            if option == 'upload':
                sftp.put(localpath, remotepath)
                sftp.close()
                transport.close()
            elif option == 'download':
                sftp.get(remotepath,localpath)
                sftp.close()
                transport.close()


        def ftp(self,user,passwd,Host,remotepath,localpath,option):
            ftp = ftplib.FTP(Host,user,passwd)
            filename = localpath.split('/')[-1]
            localdir = localpath.replace(filename,'')
            remotepath = remotepath.replace(filename,'')
            ftp.cwd(remotepath)  #Change FTP server directory
            if option == 'upload':
                upfile = open(localpath, 'rb')
                ftp.storbinary('STOR '+filename, upfile)
                upfile.close()
            elif option == 'download':
                with open(localpath, 'wb') as f:
                    ftp.retrbinary('RETR ' + filename, f.write)


        def mail(self):
            body = open(self.MailBody, 'r')
            content = MIMEText(body.read(), self.MailFormat ,'utf-8')
            msg = MIMEMultipart('alternative')
            msg['Subject'] =  self.MailSubject
            msg['From'] = self.MailSender
            msg['To'] = self.Recipient
            msg.attach(content)
            if self.MailFiles == None:pass
            else:
                for message in self.MailFiles:
                    if re.search('.xlsx',message): #XLSX documents
                        f = open(message,'rb')
                        attachment = MIMEBase('application', 'vnd.ms-excel')
                        attachment.set_payload(f.read())
                        attachment.add_header('Content-Disposition', 'attachment', filename=message.split('/')[-1])
                        f.close()
                        msg.attach(attachment)
                        encoders.encode_base64(attachment)
                    if re.search('.txt',message): #Text documents
                        f = open(message,'r')
                        attachment = MIMEText(f.read())
                        attachment.add_header('Content-Disposition', 'attachment', filename=message.split('/')[-1])
                        f.close()
                        msg.attach(attachment)
                    if re.search('.tar.gz',message): #Tar Files
                        f = open(message,'rb')
                        attachment = MIMEBase('application', 'x-gzip')
                        attachment.set_payload(f.read())
                        attachment.add_header('Content-Disposition', 'attachment', filename=message.split('/')[-1])
                        f.close()
                        msg.attach(attachment)
                        encoders.encode_base64(attachment)
            s = smtplib.SMTP('localhost')
            s.sendmail(self.MailSender, self.Recipient.split(), msg.as_string())


        def commandsdef(self,software, Host):   #Commands based on software
            if software == 0:
                commands = self.alucommands     #ALU
                software = 'ALU'
            elif software == 1:
                commands = self.xrcommands    #XR
                software = 'XR'
            elif software == 2:
                commands = self.ioscommands   #IOS
                software = 'IOS'
            elif software == 3:
                commands = self.junoscommands #Junos
                software = 'JUNOS'
            elif software == 4:
                commands = self.asacommands   #ASA
                software = 'ASA'
            elif software == 5:
                commands = self.linuxcommands #Linux
                software = 'LINUX'
            else:
                print(Host + " - No software detected...")
                commands = "1"
            data = {'commands':commands,'software':software}
            return data


        def counter(self):
            print("\n\n######################")
            print("# SSH Counter = " + str(self.sshcounter.value))
            print("# Telnet Counter = " + str(self.telnetcounter.value))


        def dates():
            now = date.today()
            ynow = now - timedelta(days=1)
            year = now.strftime('%Y')
            month = now.strftime('%m')
            day = now.strftime('%d')
            yyear = ynow.strftime('%Y')
            ymonth = ynow.strftime('%m')
            yday = ynow.strftime('%d')
            today = month + "_" + day + "_" + year
            yesterday = ymonth + "_" + yday + "_" + yyear
            data = {'today':today,'yesterday':yesterday,'year':year,'month':month,'day':day}
            return data
