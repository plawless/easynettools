#!/usr/bin/python3
import time
import getpass
from concurrent import futures
from multiprocessing import Pool
from multiprocessing import Manager
from EasyNetTools import EasyNetTools
EasyNetTools = EasyNetTools.EasyNetTools

"""
This is a template for ssh scripts only, run in Python3

Contact: patrick.lawless@charter.com
"""

Username = input("Username: ")
Password = getpass.getpass()
devicelist = input("Device list file: ")
hostnames = open(devicelist, 'r')
configs = input("Are you making config changes? [y/n] ")
if "y" in configs: CommandDelay = .5
else:CommandDelay = 5

##### Variables #####
max_workers = 300  #How many Process's or Threads to be allowed (Careful)
WorkingDir = "/home/user/"  #Location of command files, ect

##### Key Word Arguments to be sent to INIT remove itmes not in use
kwargs = ({'Username':Username,'Password':Password,'CommandDelay':CommandDelay,
    'WorkingDir':WorkingDir})
tools = EasyNetTools(**kwargs)

##### Global INIT Variables initalized
manager = Manager()  #Needed to append in shared memory
report = manager.list([])
CommandFile = tools.CommandFile()


def Work(host):
    try:
        host=host.strip('\n')
        session = tools.Login(host)#Login via SSH/Telnet
        send = session['send']
        list1 = list(['\n\n*************** ' + host + ' ***************\n\n', ])
        for line in session['commands']: # Send a list of commands from a command file
            send(line.encode().strip() + b'\r')
            time.sleep(CommandDelay)
        if session['read'] == 1:output = session['session'].recv(500000).decode().split('\n') #SSH read
        else:output = session['session'].read_very_eager().decode().split('\n')               #Telnet read
        for l in output: ##### Strip any keywords we dont want to see and append to local list
            if any(n in l for n in session['dontprint']):continue
            list1.append(l)
    except: ##### On Failure, Log device
        list1 = list(['\nCould not log in to ' + host])

    report.append(list1)  #Append local list to global list




###### Start of SSH/Telnet Pool Script ######
scripttime = CommandFile['scripttime']
print("Estimated time for script to compleate is " + str(scripttime) + " sec")
concurrent = futures.ThreadPoolExecutor(max_workers)    #Threading
#concurrent = futures.ProcessPoolExecutor(max_workers)  #Process

with concurrent as ex:
    ex.map(Work, hostnames)

hostnames.close()

###### Print output of commands and post processing ######
report = sorted(report, key=lambda hostname: hostname[0])
for y in report:
        for x in y: print(x)
tools.counter()
