#!/usr/bin/python3
import time
import getpass
import multiprocessing
from functools import partial
from concurrent import futures
from multiprocessing import Pool
from multiprocessing import Manager
from EasyNetTools import EasyNetTools
EasyNetTools = EasyNetTools.EasyNetTools

"""
This is a template for ssh scripts only, run in Python3

Contact: patrick.lawless@charter.com
"""

Username = input("Username: ")
Password = getpass.getpass()
devicelist = input("Device list file: ")
hostnames = open(devicelist, 'r')
configs = input("Are you making config changes? [y/n] ")
if "y" in configs: CommandDelay = .5
else:
    CommandDelay = 5
##### Variables #####
WorkingDir = "/home/P2179415/scripts/ssh-paramiko/test/"  #Location of command files, ect
##### Key Word Arguments to be sent to INIT remove itmes not in use
kwargs = ({'Username':Username,'Password':Password,'CommandDelay':CommandDelay,
    'WorkingDir':WorkingDir})
tools = EasyNetTools(**kwargs)
##### Global INIT Variables initalized
CommandFile = tools.CommandFile()
devices =[]
for l in hostnames:
    l=l.strip('\n')
    devices.append(l)
hostnames.close()


def Work(report,failreport,host):
    try:
        host=host.strip('\n')
        host=host.split(' ')[0]
        session = tools.Login(host)#Login via SSH/Telnet
        send = session['send']
        list1 = list(['\n\n*************** ' + host + ' ***************\n\n', ])
        for line in session['commands']: # Send a list of commands from a command file
            send(line.encode().strip() + b'\r')
            time.sleep(CommandDelay)
####  OLD Way of processing output - updated to new expect function (vty_output)
#            if session['read'] == 1:output = session['session'].recv(500000).decode().split('\n') #SSH read
#            else:output = session['session'].read_very_eager().decode().split('\n')               #Telnet read
#            for l in output: ##### Strip any keywords we dont want to see and append to local list
            for l in tools.vty_output(session):
                if any(n in l for n in session['dontprint']):continue
                list1.append(l)
        report.append(list1)
    except: ##### On Failure, Log device
        list1 = list(['Could not log in to - ' + host])
        failreport.append(list1)


def thread_set(report,failreport,data):
    #Each Process is given 500 threads to work with
    max_workers = 500
    concurrent = futures.ThreadPoolExecutor(max_workers)
    mapfunc_01 = partial(Work,report,failreport)
    #Start of actual script
    with concurrent as ex:
        ex.map(mapfunc_01, data)


def divide_list(devices,total): 
    for l in range(0, len(devices), total):
        yield devices[l:l + total] 


###### Start of SSH/Telnet Pool Script ######
scripttime = CommandFile['scripttime']
print("Estimated time for script to compleate is " + str(scripttime) + " sec")
CPUS = multiprocessing.cpu_count()
manager = multiprocessing.Manager()
report = manager.list()
failreport = manager.list()
#Split the amount of devices into equal lists for each process
total = len(devices)/CPUS
total = int(round(total))
if total < 1:
    total=1
devlist = list(divide_list(devices, total)) 

mapfunc = partial(thread_set,report,failreport)
with multiprocessing.Pool(processes=CPUS, maxtasksperchild=None) as pool:
    for result in pool.imap_unordered(mapfunc, devlist):
        pass

###### Print output of commands and post processing ######
report = sorted(report, key=lambda hostname: hostname[0])
for y in report:
        for x in y: print(x)
print('''\n\n\n#############################################
               \r################ Fail Report ################\n''')
for y in failreport:
        for x in y: print(x)
tools.counter()
